#include <iostream>
#include <unistd.h> //getopt()
#include "arguments.h"

using namespace std;

//how to get link local address: https://valileo-valilei.blogspot.com/2010/09/getting-link-local-addres-from.html
int get_link_local_address(const string &interface_name, size_t interface_name_length, sockaddr_in6 *ipv6) {
    ifaddrs *first_address, *address;
    sockaddr_in6 *current_addr;
    //set implicitly return value to -1 = link-local address not found
    int retval = -1;

    //get list of network interfaces if the local system
    if (getifaddrs(&first_address) < 0) {
        freeifaddrs(first_address);
        cerr << "FAIL: getifaddrs returned -1" << endl;
        cerr << strerror(errno) << endl;
        exit(EXIT_FAILURE);
    }

    //iterate through list of addresses
    for (address = first_address; address != nullptr; address = address->ifa_next) {
        //if the address is IPv6 and interface is the same as needed
        if (address->ifa_addr->sa_family == AF_INET6 &&
            strncmp(address->ifa_name, interface_name.c_str(), interface_name_length) == 0) {
            current_addr = (sockaddr_in6 *) address->ifa_addr;
            //and if it's link local adress
            if (IN6_IS_ADDR_LINKLOCAL(&(current_addr->sin6_addr))) {
                //store address and return 0 => address found
                memcpy(ipv6, current_addr, sizeof(*current_addr));
                retval = 0;
            }
        }
    }
    freeifaddrs(first_address);
    return retval;
}

int main(int argc, char *argv[]) {

    Arguments arguments;
    arguments.parse(argc, argv);

//    cout << "Argument -i:\t" << arguments.get_interface_name() << endl;
//    cout << "Argument -r:\t" << arguments.get_address() << "/" << arguments.get_prefix_length() << endl;
//    cout << "Argument -m:\t" << arguments.get_metric() << endl;
//    cout << "Argument -n:\t" << arguments.get_next_hop() << endl;
//    cout << "Argument -t:\t" << arguments.get_router_tag() << endl;

    int fd;
    cout << "Creating socket... ";
    //create socket in IPv6 domain with UDP communication semantics
    if ((fd = socket(AF_INET6, SOCK_DGRAM, 0)) < 0) {
        cerr << "FAIL: socket() returned -1" << endl;
        cerr << strerror(errno) << endl;
        exit(EXIT_FAILURE);
    }
    cout << "OK" << endl;

    cout << "Finding link-local address... ";
    sockaddr_in6 src_address{};
    memset(&src_address, 0, sizeof(src_address));
    //link-local address is needed to be accepted by router
    //(RFC 2080: the source of the datagram must be a link-local address...)
    if (get_link_local_address(arguments.get_interface_name(), sizeof(arguments.get_interface_name().c_str()),
                               &src_address) == -1) {
        cerr << "FAIL: get_link_local_address() returned -1, link-local address not found" << endl;
        exit(EXIT_FAILURE);
    }
    src_address.sin6_family = AF_INET6;
    src_address.sin6_port = htons(RIPNG_PORT);
    char src[INET6_ADDRSTRLEN];
    cout << "OK" << endl;
    cout << "\tLink-local used: " << inet_ntop(AF_INET6, &(src_address.sin6_addr), src, INET6_ADDRSTRLEN) << endl;

    if (bind(fd, (struct sockaddr *) &src_address, sizeof(src_address)) < 0) {
        perror("FAIL: bind() returned -1");
        cerr << strerror(errno) << endl;
        close(fd);
        exit(EXIT_FAILURE);
    }

    //packet must have hop-count 255, to be accepted by router
    //(RFC 2018: periodic advertisements must have their hop counts set to 255...)
    int hops = 255;
    if (setsockopt(fd, IPPROTO_IPV6, IPV6_MULTICAST_HOPS, &hops, sizeof(hops)) < 0) {
        cerr << "FAIL: setsockopt with IPV6_MULTICAST_HOPS returned -1" << endl;
        cerr << strerror(errno) << endl;
        close(fd);
        exit(EXIT_FAILURE);
    }

    cout << "Crafting packet... ";
    sockaddr_in6 dest_address{};
    memset(&dest_address, 0, sizeof(dest_address));
    dest_address.sin6_family = AF_INET6;
    dest_address.sin6_port = htons(RIPNG_PORT);
    inet_pton(AF_INET6, RIPNG_MULTICAST, &dest_address.sin6_addr);

    riphdr header{};
    ripngdata address_data{};
    ripngdata next_hop_data{};

    header.command = 2;
    header.version = 1;
    memset(&header.must_be_zero, 0, sizeof(header.must_be_zero));

    in6_addr address{};
    //we checked address validity during the argument parsing
    //convert IPv6 address in string format to network format
    inet_pton(AF_INET6, arguments.get_address().c_str(), &address);
    address_data.prefix = address;
    address_data.prefix_length = static_cast<uint8_t>(arguments.get_prefix_length());
    address_data.metric = static_cast<uint8_t>(arguments.get_metric());
    address_data.route_tag = static_cast<uint16_t>(arguments.get_router_tag());

    //RIPng packet has one header and two entries = next hop + spurious route
    char ripng_packet[RIP_HLEN + RIPNG_DLEN + RIPNG_DLEN];
    //copy header to the beggining of the packet
    memcpy(ripng_packet, &header, sizeof(header));

    //if there's some next-hop valid entry
    if (arguments.get_next_hop() != "::") {
        inet_pton(AF_INET6, arguments.get_next_hop().c_str(), &address);
        next_hop_data.prefix = address;
        //fill "must-be-zero" fields with zeros
        memset(&next_hop_data.prefix_length, 0, sizeof(next_hop_data.prefix_length));
        memset(&next_hop_data.route_tag, 0, sizeof(next_hop_data.route_tag));
        //next hop metric is set to 0xFF (255) to be correctly accepted by router
        next_hop_data.metric = NEXT_HOP_METRIC;
        //append next hop entry and then spurious route entry to the packet
        memcpy(&ripng_packet[RIP_HLEN], &next_hop_data, sizeof(ripng_packet));
        mempcpy(&ripng_packet[RIP_HLEN + RIPNG_DLEN], &address_data, sizeof(ripng_packet));
    } else
        //or append just spurious route entry if there's no next hop entry
        memcpy(&ripng_packet[RIP_HLEN], &address_data, sizeof(ripng_packet));
    cout << "OK" << endl;

    cout << "Sending... " << endl;
    if (sendto(fd, ripng_packet, sizeof(ripng_packet), 0, (struct sockaddr *) &dest_address, sizeof(dest_address)) <
        0) {
        cerr << "FAIL: sendto() returned -1" << endl;
        cerr << strerror(errno) << endl;
        close(fd);
        exit(EXIT_FAILURE);
    }
    cout << endl << "Spurious RIPng packet has been sent." << endl;
}