#ifndef RIP_CPP_OPTION_H
#define RIP_CPP_OPTION_H

#include <string>
#include <unistd.h>         //getopt()
#include <iostream>
#include <arpa/inet.h>      //inet_pton(), htonl(), in6_addr...
#include "ripng_packet.h"

#define ARGUMENTS_MAX 11
#define ARGUMENTS_MIN 2
#define METRIC_MAX 16
#define METRIC_MIN 0
#define ROUTER_TAG_MAX 65535
#define ROUTER_TAG_MIN 0
#define PREFIX_LENGTH_MAX 128
#define PREFIX_LENGTH_MIN 16

using namespace std;

class Arguments {

    string interface_name;

    string address;

    int prefix_length;

    string next_hop = "::";

    int metric = 1; //[0-16]

    int router_tag = 0; //[0-65535]

public:

    /**
     * @brief Print usage to standard error (used in case of error handling)
     */
    static void print_usage();

    /**
     * @brief Parse program arguments and set class members
     * @param argc Argument count
     * @param argv Array containing program arguments
     */
    void parse(int argc, char **argv);

    /**
     * @brief Parse IPv6 address to prefix and prefix length, and set the class members
     * @param address_str IPv6 address string
     */
    void parse_address(string address_str);

    const string &get_interface_name() const;

    const string &get_address() const;

    const string &get_next_hop() const;

    int get_metric() const;

    int get_router_tag() const;

    int get_prefix_length() const;
};


#endif //RIP_CPP_OPTION_H
