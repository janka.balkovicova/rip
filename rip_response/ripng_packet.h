#ifndef RIP_CPP_RIPNG_H
#define RIP_CPP_RIPNG_H

#include <sys/socket.h>     //socket(), bind(), macros
#include <net/if.h>         //ifnameindex()...
#include <arpa/inet.h>      //inet_pton(), htonl(), in6_addr...
#include <ifaddrs.h>        //ifaddrs(), getifaddrs()...
#include <cstring>
#include <netinet/in.h>     //in_addr...
#include <pcap.h>

#define RIP_HLEN 4
#define RIPNG_DLEN 20
#define RIPNG_PORT 521
#define RIPNG_MULTICAST "ff02::9"

#define NEXT_HOP_METRIC 255

/**
 * @brief RIP header structure used for crafting RIPng packet
 */
struct riphdr {
    uint8_t command; //1-request, 2-response
    uint8_t version;
    uint16_t must_be_zero;
};

/**
 * @brief RIPng data (entry) structure used for crafting RIPng packet
 */
struct ripngdata {
    in6_addr prefix;
    uint16_t route_tag;
    uint8_t prefix_length;
    uint8_t metric;
};

#endif //RIP_CPP_RIPNG_H
