#include "arguments.h"

const string &Arguments::get_interface_name() const {
    return interface_name;
}

const string &Arguments::get_address() const {
    return address;
}

const string &Arguments::get_next_hop() const {
    return next_hop;
}

int Arguments::get_metric() const {
    return metric;
}

int Arguments::get_router_tag() const {
    return router_tag;
}

int Arguments::get_prefix_length() const {
    return prefix_length;
}

void Arguments::parse(int argc, char **argv) {

    //don't continue if argument count is too large or too small
    if (argc < ARGUMENTS_MIN || argc > ARGUMENTS_MAX) {
        print_usage();
        exit(EXIT_FAILURE);
    }

    //https://www.gnu.org/software/libc/manual/html_node/Getopt.html#Getopt
    int c;
    while ((c = getopt(argc, argv, "i:r:m:n:t:")) != -1) {
        switch (c) {
            case 'i':
                interface_name = string(optarg);
                break;

            case 'r':
                parse_address(string(optarg));
                break;

            case 'm': {
                int m = stoi(optarg);
                if (m < METRIC_MIN || m > METRIC_MAX) {
                    cerr << "Error: Metric: [0-16]" << endl;
                    exit(EXIT_FAILURE);
                } else metric = m;
                break;
            }

            case 'n': {
                sockaddr_in6 addr{};
                //use conversion for checking if the address is valid
                if (inet_pton(AF_INET6, optarg, &(addr.sin6_addr)) < 1) { //inet_pton returns 1 on success
                    cerr << "Error: Invalid IPv6 next hop address." << endl;
                    print_usage();
                    exit(EXIT_FAILURE);
                } else next_hop = optarg;
                break;
            }

            case 't': {
                int t = stoi(optarg);
                if (t < ROUTER_TAG_MIN || t > ROUTER_TAG_MAX) {
                    cerr << "Error: Router tag: [0-65535]" << endl;
                    exit(EXIT_FAILURE);
                } else router_tag = t;
                break;
            }

            case '?':
                if (optopt == 'i' || optopt == 'r' || optopt == 'm' || optopt == 'n' || optopt == 't' ||
                    optopt == 'r') {
                    cerr << "Error: Option" << (char) optopt << " requires an argument." << endl;
                } else if (isprint(optopt)) {
                    cerr << "Error: Unknown option " << (char) optopt << endl;
                } else {
                    cerr << "Error: Unknown option character " << optopt << endl;
                }

            default:
                cerr << strerror(errno) << endl;
                print_usage();
                exit(EXIT_FAILURE);
        }
    }

    //if user did not enter needed values
    if (interface_name.empty() || address.empty()) {
        cerr << "Error: Mandatory argument missing." << endl;
        print_usage();
        exit(EXIT_FAILURE);
    }

    int index;
    for (index = optind; index < argc; index++) {
        cerr << "Error: Redundant argument found:" << argv[index] << endl;
        print_usage();
        exit(EXIT_FAILURE);
    }
}

void Arguments::print_usage() {
    cerr << "Usage: " << endl;
    cerr << "./myripresponse -i <interface> -r <IPv6>/[16-128] {-n <IPv6>} {-m [0-16]} {-t [0-65535]}" << endl;
    cerr << "\t-i: interface used for sending packet" << endl;
    cerr << "\t-r: <IPv6> spurious route" << endl;
    cerr << "\t-n: <IPv6> next-hop address for spurious route, implicitly ::" << endl;
    cerr << "\t-m: hop count, implicitly 1" << endl;
    cerr << "\t-t: router tag implicitly 0" << endl;
}

void Arguments::parse_address(string address_str) {
    //we need to separate string before '/' and after
    auto delimiter = address_str.find('/');
    string ipv6 = address_str.substr(0, delimiter); //save ipv6 address substring
    //use conversion for checking if the address is valid
    sockaddr_in6 addr{};
    if (inet_pton(AF_INET6, ipv6.c_str(), &(addr.sin6_addr)) < 1) { //inet_pton returns 1 on success
        cerr << "Error: Invalid IPv6 address." << endl;
        print_usage();
        exit(EXIT_FAILURE);
    } else address = ipv6; //if it is, save it to class member

    //erase address + delimiter => what's left is prefix length string
    string prefix_str = address_str.erase(0, delimiter + 1);
    //convert prefix length to integer and check if it's correct
    int prefix = stoi(prefix_str);
    if (prefix < PREFIX_LENGTH_MIN || prefix > PREFIX_LENGTH_MAX) {
        cerr << "Error: IPv6 prefix length: [16-128]" << endl;
        print_usage();
        exit(EXIT_FAILURE);
    } else prefix_length = prefix; //if it is, save it to class member
}
