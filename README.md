# RIP sniffer and RIPng Response sender
## Description
Sniffer and RIPng Response sender are tools for monitoring and generating messegases of simple distance-vector protocols implemented in C++. They are meant to be a part of the project for university course Network Applications and Network Administration in academic year 2018-2019.

## Compilation & Usage
Each program has its own folder, in which Makefile is present. To compile specific program just execute `make` in that directory. To clean executable and linked objects execute `make clean` in that directory.

------------

### myripsniffer
For compiling `myripsniffer` run following commands from the root directory of the project:
``` sh
$ cd sniffer
$ make
```
Run program by executing
``` sh
$ sudo ./myripsniffer -i <interface>
``` 
where:
- **-i** interface used for sniffing packets

The program uses promiscuous mode of a network card, which requires root access. Also, `libpcap` library is needed.

------------

### myripresponse
For compiling `myripresponse` run following commands from the root directory of the project:
```sh
$ cd response
$ make
```
Run program by executing
```sh
$ sudo ./myripresponse -i <interface> -r <IPv6>/[16-128] {-n <IPv6>} {-m [0-16]} {-t [0-65535]}
```
where:
- **-i** interface from which is spurious packet sent
- **-r** IPv6 spurious route
- **-m** hop count of the route (1 by default)
- **-n** next hop router addres (:: by default)
- **-t** route tag (0 by default)

This programs socket binds to "privileged" RIPng port 521 (which is below 1024), therefore the root access is required as well.

## Examples
I have connected my computer through host-only adapter to virtual router running on VirtualBox. I start the virtual machine in VirtualBox, check if virtual network *vboxnet0* is receiving packets in [Wireshark](https://www.wireshark.org/), and I start `myripsniffer`.
``` sh
$ sudo ./myripsniffer -i virboxn0
```
Output:
```
Opening interface vboxnet0 with net address 192.168.56.0 (mask 255.255.255.0) for listening... OK. 
Applying filter: portrange 520-521 and udp... OK. 
Starting sniffing... 
[14:49:10 19-11-2018, no. 1] ------------------------------------ 
-- Ethernet, Src: 8:0:27:6d:35:14, Dst: 33:33:0:0:0:9 
-- Internet Protocol Version 6, Src: fe80::a00:27ff:fe6d:3514, Dst: ff02::9 
-- User Datagram Protocol, Src Port: 521, Dst Port: 521 
-- RIPng 
	Command: Response (2) 
	Version: 1 
	[Entry no. 0] 
		IPv6 Prefix: fd00:: 
		Route Tag: 0 
		Prefix Length: 64 
		Metric: 1 
	[Entry no. 1] 
	IPv6 Prefix: fd00:db:3408:: 
		Route Tag: 0 
		Prefix Length: 64 
		Metric: 1 
	[Entry no. 2] 
		IPv6 Prefix: fd00:fc:28f6:: 
		Route Tag: 0 
		Prefix Length: 64 
		Metric: 1 
	[Entry no. 3] 
		IPv6 Prefix: fd00:930:1291:: 
		Route Tag: 0 
		Prefix Length: 64 
		Metric: 1 
	[Entry no. 4] 
		IPv6 Prefix: fd00:948:62:: 
		Route Tag: 0 
		Prefix Length: 64 
		Metric: 1 
		
 [14:49:34 19-11-2018, no. 2] ------------------------------------
```
and so on.

While `myripsniffer` is running, I send RIPng Response message by executing...

``` sh
$ sudo ./myripresponse -i virboxn0 -r 2001:db8:0:abcd::/64 -n fe80::abcd
```

`myripresponse` output:
``` 
Creating socket... OK 
Finding link-local address... OK 
    Link-local used: fe80::800:27ff:fe00:0 
Crafting packet... OK 
Sending... 

Spurious RIPng packet has been sent.
```
...and check `myripsniffer` output:
```
[15:35:33 19-11-2018, no. 7] ------------------------------------
-- Ethernet, Src: a:0:27:0:0:0, Dst: 33:33:0:0:0:9
-- Internet Protocol Version 6, Src: fe80::800:27ff:fe00:0, Dst: ff02::9
-- User Datagram Protocol, Src Port: 521, Dst Port: 521
-- RIPng
	Command: Response (2)
	Version: 1
	[Entry no. 0]
		IPv6 Prefix: fe80::abcd
		Route Tag: 0
		Prefix Length: 0
		Metric: 255
	[Entry no. 1]
		IPv6 Prefix: 2001:db8:0:abcd::
		Route Tag: 0
		Prefix Length: 64
		Metric: 1
```
------------
### tcpreplay

If you do not have access to similar virtual machine, you can use `tcpreplay` tool with .cap/.pcap/.pcapng files, which contains captured network traffic. In root folder there are example captures containing RIPv1, RIPv2 (MD5 autenthication present) and RIPng protocols, that can be used to test `myripsniffer`. \
To run `tcpreplay`, you must specify the interface and file you want to use, for example:
```
$ tcpreplay --intf1=eth0 RIPv1.cap
```
where *eth0* is the interface and *RIPv1.cap* is file with captured traffic.

----------
Further information about the routing tables and routing protocols can be found below:

[Routing Table](https://searchnetworking.techtarget.com/definition/routing-table) \
[RIP protocol basics](https://searchnetworking.techtarget.com/definition/Routing-Information-Protocol) \
[RIP tutorial](https://www.9tut.com/rip-routing-protocol-tutorial) \
[Comparison: RIPv1 vs. RIPv2](http://www.networksorcery.com/enp/protocol/rip.htm) \
[RIPng Overview](https://www.juniper.net/documentation/en_US/junos/topics/concept/routing-protocol-rip-ng-security-overview.html)
