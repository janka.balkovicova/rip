#ifndef RIP_CPP_SNIFFER_H
#define RIP_CPP_SNIFFER_H

// struct in_addr,
#include <netinet/in.h>
// inet_ntoa
#include <arpa/inet.h>
#include <pcap.h>
#include <iostream>
#include <cstring>

#include "ripng_packet.h"

#define PROMISCUOUS_MOD 1
#define BUFFER_TIMEOUT 1000

using namespace std;

class Sniffer {

    char err_buff[PCAP_ERRBUF_SIZE]{};

    string device;

    pcap_t *handle;

    bpf_u_int32 net;

    bpf_u_int32 mask;

    bpf_program fp;

    string filter_expression = "portrange 520-521 and udp"; //RIP port = 520, RIPng port = 521, both on UDP

    /**
     * @brief Set filter to filter traffic going through device
     */
    void set_filter();

    /**
     * @brief Open device in promiscuous mode to sniffing
     */
    void open_device();

    /**
     * @brief Set net address and mask for print to standard output
     */
    void set_net_mask();

    /**
     * @brief Implementation of callback function, parse RIP and RIPng packet
     */
    static void parse_packet(u_char *args, const pcap_pkthdr *pcap_header, const u_char *pcap_packet);

public:

    void set_device(const string &dev);

    /**
     * @brief Start sniffing
     */
    void start();
};

/**
 * @brief Static counter used for displaying order in which packets came
 */
static int packet_counter = 1;

#endif //RIP_CPP_SNIFFER_H
