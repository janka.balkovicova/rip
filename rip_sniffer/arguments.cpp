#include "arguments.h"

const string &Arguments::get_interface() const {
    return interface_name;
}

void Arguments::print_usage() {
    cerr << "Usage: " << endl;
    cerr << "./myripsniffer -i <interface>" << endl;
    cerr << "\t-i: <interface> interface used for sniffing packets" << endl;
}

void Arguments::parse(int argc, char **argv) {
    if (argc != ARGUMENTS_COUNT) {
        cerr << "Error: Wrong arguments passed." << endl;
        print_usage();
        exit(EXIT_FAILURE);
    }

    //https://www.gnu.org/software/libc/manual/html_node/Getopt.html#Getopt
    int c;
    while ((c = getopt(argc, argv, "i:")) != -1) {
        switch (c) {
            case 'i':
                interface_name = string(optarg);
                break;

            case '?':
                if (optopt == 'i') {
                    cerr << "Error: Invalid argument." << endl;
                } else if (isprint(optopt))
                    cerr << "Error: Invalid option: '" << (char) optopt << "'" << endl;
                else
                    cerr << "Error: Wrong arguments passed." << endl;

            default:
                print_usage();
                exit(EXIT_FAILURE);
        }
    }
}