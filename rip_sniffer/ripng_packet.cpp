#include "ripng_packet.h"

RIPngPacket::RIPngPacket(int number, ether_header *ethernet_header, ipv6hdr *ip_header, udphdr *udp_header,
                         riphdr *ripng_header,
                         ripngdata *ripng_data) : number(number), ethernet_header(ethernet_header),
                                                  ip_header(ip_header),
                                                  udp_header(udp_header), rip_header(ripng_header),
                                                  ripng_data(ripng_data) {}

void RIPngPacket::print() {
    //https://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
    time_t now = time(0);
    tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%X %d-%m-%Y", &tstruct);

    cout << "[" << buf << ", no. " << number << "] ------------------------------------" << endl;
    cout << "-- Ethernet, Src: " << ether_ntoa((const struct ether_addr *) &ethernet_header->ether_shost) << ", Dst: "
         << ether_ntoa((const struct ether_addr *) &ethernet_header->ether_dhost) << endl;
    char str[INET6_ADDRSTRLEN];
    //ged rid of everything before IP version, shift by 4 bits to get IP version on correct index
    //inet_ntop converts IPv6 to printable form (char array)
    cout << "-- Internet Protocol Version " << ((this->ip_header->metadata & 0x000000FF) >> 4) << ", Src: "
         << inet_ntop(AF_INET6, &(ip_header->src), str, INET6_ADDRSTRLEN) << ", Dst: "
         << inet_ntop(AF_INET6, &(ip_header->dest), str, INET6_ADDRSTRLEN) << endl;
    cout << "-- User Datagram Protocol, " << "Src Port: " << dec << ntohs(udp_header->uh_sport) << ", Dst Port: " << dec
         << ntohs(udp_header->uh_dport) << endl;

    ripng_len = ntohs(udp_header->len) - UDP_HLEN; //UDP packet length - UDP header => the rest is RIPng packet
    int entry_number = 0;
    cout << "-- RIPng" << endl;
    RIPPacket::print_command((int) rip_header->command);
    RIPPacket::print_version((int) rip_header->version);

    //start by skipping RIP header, iterate through packet data and print it's entries
    for (ripng_len -= RIP_HLEN; ripng_len >= RIP_ELEN; ripng_len -= RIP_ELEN) {
        cout << "\t[Entry no. " << entry_number++ << "]" << endl;
        cout << "\t\tIPv6 Prefix: " << inet_ntop(AF_INET6, &(ripng_data->prefix), str, sizeof(str)) << endl;
        cout << "\t\tRoute Tag: " << (int) ntohs(ripng_data->route_tag) << endl;
        cout << "\t\tPrefix Length: " << (int) ripng_data->prefix_length << endl;
        cout << "\t\tMetric: " << (int) ripng_data->metric << endl;

        //move pointer to the next entry
        ripng_data = (ripngdata *) ((u_char *) ripng_data + RIP_ELEN); //current pointer + entry length
    }
    cout << endl;
}
