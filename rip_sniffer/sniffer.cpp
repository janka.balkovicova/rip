#include "sniffer.h"

void Sniffer::set_device(const string &dev) {
    Sniffer::device = dev;
}

void Sniffer::open_device() {
    set_net_mask(); //for displaying device's address and mask

    in_addr net_addr{};
    net_addr.s_addr = net;

    in_addr mask_addr{};
    mask_addr.s_addr = mask;

    cout << "Opening interface " << device << " with net address " << inet_ntoa(net_addr) << " (mask "
         << inet_ntoa(mask_addr) << ") for listening... ";

    handle = pcap_open_live(device.c_str(), BUFSIZ, PROMISCUOUS_MOD, BUFFER_TIMEOUT, err_buff);
    if (handle == nullptr) {
        cerr << "Could not open device: " << err_buff << endl;
        exit(EXIT_FAILURE);
    }

    cout << "OK." << endl;
}

void Sniffer::set_net_mask() {
    if (pcap_lookupnet(device.c_str(), &net, &mask, err_buff) < 0) {
        cerr << "Cannot get net address and mask for device: " << device << endl;
        cerr << strerror(errno) << endl;
        net = 0;
        mask = 0;
    }
}

void Sniffer::set_filter() {
    cout << "Applying filter: " << filter_expression << "... ";

    if (pcap_compile(handle, &fp, filter_expression.c_str(), 0, net) < 0) {
        cerr << "Could not parse the filter: " << filter_expression << endl << pcap_geterr(handle) << endl;
        exit(EXIT_FAILURE);
    }

    if (pcap_setfilter(handle, &fp) < 0) {
        cerr << "Could not install the filter: " << filter_expression << endl << pcap_geterr(handle) << endl;
        exit(EXIT_FAILURE);
    }

    cout << "OK." << endl;
}

void Sniffer::start() {
    open_device(); //open interface in promiscuous mode
    set_filter(); //filter traffic

    cout << "Starting sniffing..." << endl;

    pcap_loop(handle, 0, parse_packet, nullptr); //call function parse_packet in loop forever

    pcap_freecode(&fp);
    pcap_close(handle);

}

void Sniffer::parse_packet(u_char *args, const pcap_pkthdr *pcap_header, const u_char *pcap_packet) {

    //create temp variables, later used in contructor
    ether_header *ethernet_header = nullptr;
    ip *ip_header = nullptr;
    u_int ip_hlen = 0;
    ipv6hdr *ipv6_header = nullptr;
    udphdr *udp_header = nullptr;
    riphdr *rip_header = nullptr;
    ripdata *rip_data = nullptr;
    ripngdata *ripng_data = nullptr;

    ethernet_header = (ether_header *) pcap_packet;

    switch (ntohs(ethernet_header->ether_type)) {

        //if ethernet frame contains IPv4 packet, set pointers to structures
        case ETHERTYPE_IP: {
            ip_header = (ip *) (pcap_packet + ETH_HLEN);
            ip_hlen = ip_header->ip_hl * 4;
            udp_header = (udphdr *) ((u_char *) ip_header + ip_hlen);
            rip_header = (riphdr *) ((u_char *) udp_header + UDP_HLEN);
            rip_data = (ripdata *) ((u_char *) rip_header + RIP_HLEN);

            //construct the IPv4 packet with them
            RIPPacket rip_packet(packet_counter++, ethernet_header, ip_header, udp_header, rip_header, rip_data);
            //and print it
            rip_packet.print();
            break;
        }

            //if ethernet frame contains IPv6 packet, set pointers to structures
        case ETHERTYPE_IPV6: {
            ipv6_header = (ipv6hdr *) (pcap_packet + ETH_HLEN);
            udp_header = (udphdr *) ((u_char *) ipv6_header + IPV6_HLEN);
            rip_header = (riphdr *) ((u_char *) udp_header + UDP_HLEN);
            ripng_data = (ripngdata *) ((u_char *) rip_header + RIP_HLEN);

            //construct IPv6 packet with them
            RIPngPacket ripng_packet(packet_counter++, ethernet_header, ipv6_header, udp_header, rip_header,
                                     ripng_data);
            //and print it
            ripng_packet.print();
            break;
        }

        default:
            cerr << "Unknown version of IP packet inside Ethernet header." << endl;
            exit(EXIT_FAILURE);
    }
}
