#ifndef RIP_H
#define RIP_H

#include <netinet/ip.h>
#include <netinet/udp.h>
#include <netinet/if_ether.h>   //struct ether_header ...
#include <netinet/ether.h>      //ether_ntoa() ...
#include <time.h>
#include <arpa/inet.h>          //htons() ...

#include <pcap.h>
#include <string>
#include <iostream>
#include <iomanip>              //setfill(), setw() - printing binary data in hex format
#include <time.h>

#define UDP_HLEN 8              //UDP Header Length

#define RIP_HLEN 4              //RIP Header Length
#define RIP_C_REQUEST 1         //RIP Command: Request
#define RIP_C_RESPONSE 2        //RIP Command: Response
#define RIP_V1 1
#define RIP_V2 2
#define RIP_AUTH_START 0xFFFF
#define RIP_AUTH_SIMPLE 2       //RIP Authentication Type: Simple
#define RIP_AUTH_MD5 3          //RIP Authentication Type: Keyed Message Digest
#define RIP_AUTH_MD5_PASSWD 1   //RIP Authentication Type: Keyed Message Digest - Authentication Data
#define RIP_ELEN 20             //RIP Entry Length

using namespace std;

/**
 * @brief RIP header structure (used for correct output in sniffer)
 */
struct riphdr {
    uint8_t command;
    uint8_t version;
    uint16_t must_be_zero;
};

/**
 * @brief Union of RIP entries. Every entry in RIP protocol has 20 bytes, access to specific structure data depends on
 * the first 2 bytes of entry (0xFFFF means authentication), and then authentication is differentiated by its type
 */
union ripdata {
    struct entry {
        uint16_t afi;
        uint16_t route_tag;
        in_addr ip;
        in_addr mask;
        in_addr next_hop;
        uint32_t metric;
    } entry;
    struct authentication {
        uint16_t start;
        uint16_t type;
        u_char password[16];
    } authentication;
    struct authentication_MD5 {
        uint16_t start;
        uint16_t type;
        uint16_t digest_offset;
        uint8_t key_id;
        uint8_t data_len;
        uint32_t seq_num;
        uint64_t must_be_zero;
    } authentication_MD5;
};

class RIPPacket {

    int number;

    ether_header *ethernet_header;

    ip *ipv4_header;

    udphdr *udp_header;

    int rip_len;

    riphdr *rip_header;

    ripdata *rip_data;

public:

    /**
     * @brief Construct RIP packet and set all its members
     * @param number Order in which the packet arrived
     * @param ethernet_header Pointer to ethernet header of the packet
     * @param ipv4_header Pointer to IPv4 header of the packet
     * @param udp_header Pointer to UDP header of the packet
     * @param rip_header Pointer to RIP header of the packet
     * @param payload The rest of the RIP packet
     */
    RIPPacket(int number, ether_header *ethernet_header, ip *ipv4_header, udphdr *udp_header, riphdr *rip_header,
              ripdata *payload);

    /**
     * @brief Print structured packet data (inspired by Virtual Box)
     */
    void print();

    /**
     * @brief Print structured RIP entry
     * @param entry_number
     */
    void print_entry(int entry_number);

    /**
     * @brief Print structured authentication or authentication data
     */
    void print_authentification();

    /**
     * @brief Print RIP command
     * @param command
     */
    static void print_command(int command);

    /**
     * @brief Print RIP version
     * @param version
     */
    static void print_version(int version);

};

#endif //RIP_H
