#include <iostream>
#include "arguments.h"
#include "sniffer.h"

#include <pcap.h>

using namespace std;

int main(int argc, char *argv[]) {
    Arguments arguments;
    arguments.parse(argc, argv);

    Sniffer sniffer;
    sniffer.set_device(arguments.get_interface());
    sniffer.start();

    return 0;
}


