#ifndef RIP_CPP_RIPNG_H
#define RIP_CPP_RIPNG_H

#include "rip_packet.h"

#define IPV6_HLEN 40

/**
 * @brief Separate RIPng data fields for easier access
 */
struct ripngdata {
    in6_addr prefix;
    uint16_t route_tag;
    uint8_t prefix_length;
    uint8_t metric;
};

/**
 * @brief Separate IPv6 header fields for easier access
 */
struct ipv6hdr {
    uint32_t metadata;
    uint16_t payload_length;
    uint8_t next_header;
    uint8_t hop_count;
    in6_addr src;
    in6_addr dest;
};


class RIPngPacket {

    int number;

    ether_header *ethernet_header;

    ipv6hdr *ip_header;

    udphdr *udp_header;

    int ripng_len;

    riphdr *rip_header;

    ripngdata *ripng_data;

public:

    /**
     * @brief Construct RIPng packet and set all of its fields
     * @param number
     * @param ethernet_header
     * @param ip_header
     * @param udp_header
     * @param ripng_header
     * @param ripng_data
     */
    RIPngPacket(int number, ether_header *ethernet_header, ipv6hdr *ip_header, udphdr *udp_header, riphdr *ripng_header,
                ripngdata *ripng_data);

    /**
     * @brief Print structured packet (inspired by Virtual Box)
     */
    void print();

};


#endif //RIP_CPP_RIPNG_H
