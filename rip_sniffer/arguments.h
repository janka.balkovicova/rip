#ifndef RIP_CPP_OPTION_H
#define RIP_CPP_OPTION_H

#include <string>
#include <utility>
#include <iostream>
#include <unistd.h>

#define ARGUMENTS_COUNT 3

using namespace std;

class Arguments {

    string interface_name;

public:
    /**
     * @brief Parse program arguments and set class members
     * @param argc Argument count
     * @param argv Array containing program arguments
     */
    void parse(int argc, char **argv);

    /**
     * @brief Print usage to standard error (used in case of error handling)
     */
    void print_usage();

    const string &get_interface() const;
};

#endif //RIP_CPP_OPTION_H
