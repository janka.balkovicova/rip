#include "rip_packet.h"

using namespace std;

RIPPacket::RIPPacket(int number, ether_header *ethernet_header, ip *ipv4_header, udphdr *udp_header, riphdr *rip_header,
                     ripdata *rip_data) : number(number), ethernet_header(ethernet_header),
                                          ipv4_header(ipv4_header), udp_header(udp_header),
                                          rip_header(rip_header),
                                          rip_data(rip_data) {}

void RIPPacket::print() {
    //https://stackoverflow.com/questions/997946/how-to-get-current-time-and-date-in-c
    time_t now = time(0);
    tm tstruct;
    char buf[80];
    tstruct = *localtime(&now);
    // Visit http://en.cppreference.com/w/cpp/chrono/c/strftime
    // for more information about date/time format
    strftime(buf, sizeof(buf), "%X %d-%m-%Y", &tstruct);

    cout << "[" << buf << ", no. " << number << "] ------------------------------------" << endl;
    cout << "-- Ethernet, Src: " << ether_ntoa((const struct ether_addr *) &ethernet_header->ether_shost) << ", Dst: "
         << ether_ntoa((const struct ether_addr *) &ethernet_header->ether_dhost) << endl;
    cout << "-- Internet Protocol Version " << ipv4_header->ip_v << ", Src: " << inet_ntoa(ipv4_header->ip_src)
         << ", Dst: " << inet_ntoa(ipv4_header->ip_dst) << endl;
    cout << "-- User Datagram Protocol, " << "Src Port: " << dec << ntohs(udp_header->uh_sport) << ", Dst Port: " << dec
         << ntohs(udp_header->uh_dport) << endl;
    cout << "-- Routing Information Protocol" << endl;

    print_command((int) rip_header->command);
    print_version((int) rip_header->version);

    rip_len = ntohs(udp_header->len) - UDP_HLEN;
    int entry_number = 0;

    //start by skipping RIP header, iterate through packet data and print it's authentication or entries
    for (rip_len -= RIP_HLEN; rip_len >= RIP_ELEN; rip_len -= RIP_ELEN) {
        if (rip_data->authentication.start == RIP_AUTH_START) {
            print_authentification();
        } else {
            print_entry(entry_number++);
        }

        //pointer to the current entry + entry length => pointer to the next entry
        rip_data = (ripdata *) ((u_char *) rip_data + RIP_ELEN);
    }

    cout << endl;
}

void RIPPacket::print_entry(int entry_number) {
    cout << "\t[Entry no. " << entry_number << "]" << endl;
    cout << "\t\tAddress Family: ";

    if ((ntohs(rip_data->entry.afi)) == 2)
        cout << "IP (" << ntohs(rip_data->entry.afi) << ")" << endl;
    else
        cout << ntohs(rip_data->entry.afi) << endl;

    if (((int) rip_header->version) == 2) {
        cout << "\t\tRoute tag: " << ntohs(rip_data->entry.route_tag) << endl;
    }

    cout << "\t\tIP Address: " << inet_ntoa(rip_data->entry.ip) << endl;

    if (((int) rip_header->version) == 2) {
        cout << "\t\tNetmask: " << inet_ntoa(rip_data->entry.mask) << endl;
        cout << "\t\tNext hop: " << inet_ntoa(rip_data->entry.next_hop) << endl;
    }

    cout << "\t\tMetric: " << ntohl(rip_data->entry.metric) << endl;
}

void RIPPacket::print_authentification() {
    switch (ntohs(rip_data->authentication.type)) {

        case RIP_AUTH_SIMPLE:
            cout << "\t[Authentication]" << endl;
            cout << "\t\tAuthentication Type: Simple Password (" << dec << ntohs(rip_data->authentication.type) << ")"
                 << endl;
            cout << "\t\tPassword: " << rip_data->authentication.password << endl;
            break;

        case RIP_AUTH_MD5:
            cout << "\t[Authentication]" << endl;
            cout << "\t\tAuthentication Type: Keyed Message Digest (" << dec << ntohs(rip_data->authentication_MD5.type)
                 << ")" << endl;
            cout << "\t\tDigest Offset: " << dec << ntohs(rip_data->authentication_MD5.digest_offset) << endl;
            cout << "\t\tKey ID: " << (int) rip_data->authentication_MD5.key_id << endl;
            cout << "\t\tAuthentication Data Length: " << (int) rip_data->authentication_MD5.data_len << endl;
            cout << "\t\tSequence Number: " << ntohl(rip_data->authentication_MD5.seq_num) << endl;
            break;

        case RIP_AUTH_MD5_PASSWD:
            cout << "\t[Authentication]" << endl;
            cout << "\t\tAuthentication Data: ";
            //to print binary data, cast what it's inside the char to integer and print it as hexadecimal
            for (unsigned char i : rip_data->authentication.password) {
                //setfill() and setw() used for adding '0': 'f' -> '0f'
                cout << setfill('0') << setw(2) << hex << (unsigned int) i++;
            }
            cout << endl;
            break;

        default:
            cout << "\t[Authentication]" << endl;
            cout << "\t\tUnknown type of authentication: " << dec << ntohs(rip_data->authentication.type) << endl;
    }
}

void RIPPacket::print_command(int command) {
    cout << "\tCommand: ";
    switch (command) {
        case RIP_C_REQUEST:
            cout << "Request (" << command << ")" << endl;
            break;
        case RIP_C_RESPONSE:
            cout << "Response (" << command << ")" << endl;
            break;
        default:
            cout << "Unknown command (" << command << ")" << endl;
    }
}

void RIPPacket::print_version(int version) {
    cout << "\tVersion: ";
    switch (version) {
        case RIP_V1:
            cout << version << endl;
            break;
        case RIP_V2:
            cout << version << endl;
            break;
        default:
            cout << "Unknown version (" << version << ")" << endl;
    }
}
